package client

import (
	"bytes"
	"encoding/base64"
	"encoding/json"
	"fmt"
	"io"
	"net/http"
)

type ResponseReport struct {
	StatusCode int
	Success    bool
}

type PostRequest struct {
	InputString string `json:"inputString"`
}

type Client struct {
	client       *http.Client
	responseUrls []string
}

func NewClient(client *http.Client, urls []string) Client {
	return Client{
		client:       client,
		responseUrls: urls,
	}
}

func (c *Client) SendRequests() {
	var err error = nil
	var rep ResponseReport
	for i := 0; err == nil; i++ {
		i %= 3
		if i == 1 {
			err, rep = c.SendPostRequest(i)
		} else {
			err, rep = c.SendGetRequest(i)
		}
		if err != nil {
			fmt.Print(rep.Success, rep.StatusCode)
		}
	}
}

func (c *Client) SendPostRequest(ind int) (err error, rep ResponseReport) {
	req := PostRequest{InputString: base64.StdEncoding.EncodeToString([]byte("aiiqbibqqibfcqb9289e2fywi"))}
	jsonReq, err := json.Marshal(req)
	if err != nil {
		return
	}
	resp, err := c.client.Post((c.responseUrls)[ind], "application/json", bytes.NewBuffer(jsonReq))
	if resp != nil {
		rep.StatusCode = resp.StatusCode
	} else {
		rep.StatusCode = http.StatusRequestTimeout
	}
	if err != nil {
		rep.Success = false
		return
	}
	defer func(Body io.ReadCloser) {
		err := Body.Close()
		if err != nil {
			fmt.Println("error while closing response body")
			return
		}
	}(resp.Body)

	body, err := io.ReadAll(resp.Body)
	if err != nil {
		fmt.Println("error while reading a response")
		return
	} else {
		var output map[string]string
		_ = json.Unmarshal(body, &output)
		fmt.Printf("%s\n", output["outputString"])
	}
	return
}

func (c *Client) SendGetRequest(ind int) (err error, rep ResponseReport) {
	resp, err := c.client.Get(c.responseUrls[ind])
	if resp != nil {
		rep.StatusCode = resp.StatusCode
	} else {
		rep.StatusCode = http.StatusInternalServerError
	}
	if ind == 2 {
		if resp != nil {
			fmt.Println(true, 200)
		}
		return
	}
	if err != nil {
		fmt.Println("Error %s\n", err)
		rep.Success = false
		return
	}
	defer func(Body io.ReadCloser) {
		err := Body.Close()
		if err != nil {
			fmt.Println("error while closing response body")
			return
		}
	}(resp.Body)

	body, err := io.ReadAll(resp.Body)
	if err != nil {
		fmt.Println("error while reading a response")
		return
	} else {
		fmt.Printf("%s\n", body)
	}
	return
}
