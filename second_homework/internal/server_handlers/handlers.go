package server_handlers

import (
	"encoding/base64"
	"encoding/json"
	"io"
	"log"
	"math/rand"
	"net/http"
	"second_homework/internal/config"
	"time"
)

type Handlers struct {
	cfg *config.Config
}

func NewHandler(cfg config.Config) Handlers {
	return Handlers{cfg: &cfg}
}

func (handler *Handlers) GetApiVersion(writer http.ResponseWriter, _ *http.Request) {
	_, err := writer.Write([]byte(handler.cfg.ApiVersion))
	if err != nil {
		return
	}
}

func (handler *Handlers) GetHardOp(w http.ResponseWriter, _ *http.Request) {
	switch rand.Intn(2) {
	case 0:
		w.WriteHeader(http.StatusInternalServerError)
	case 1:
		w.WriteHeader(http.StatusOK)
	}
	time.Sleep(time.Duration(10+rand.Intn(11)) * time.Second)
}

type Request struct {
	InputString string `json:"inputString"`
}

type Response struct {
	Status        string `json:"status"`
	Error         error  `json:"error"`
	DecodedString string `json:"outputString"`
}

func DecodingBase64(inputString string) (string, error) {
	decodeString, err := base64.StdEncoding.DecodeString(inputString)
	return string(decodeString), err
}

func (handler *Handlers) PostDecode(writer http.ResponseWriter, request *http.Request) {
	defer func() {
		err := request.Body.Close()
		if err != nil {
			writer.WriteHeader(http.StatusInternalServerError)
		}
	}()

	body, err := io.ReadAll(request.Body)
	if err != nil {
		writer.WriteHeader(http.StatusInternalServerError)
		return
	}

	var req Request
	err = json.Unmarshal(body, &req)
	if err != nil {
		writer.WriteHeader(http.StatusBadRequest)
	}

	decodedString, err := DecodingBase64(req.InputString)
	if err != nil {
		writer.WriteHeader(http.StatusBadRequest)
		return
	}

	ans, err := json.Marshal(Response{
		Status:        "OK",
		Error:         err,
		DecodedString: decodedString,
	})

	if err != nil {
		writer.WriteHeader(http.StatusInternalServerError)
		return
	}
	_, err = writer.Write(ans)
	if err != nil {
		log.Fatal("failed write")
	}
	return
}
