package config

import (
	"encoding/json"
	"os"
)

type Config struct {
	Port       string `json:"port"`
	ApiVersion string `json:"Api_version"`
}

func NewConfig(filePath string) (*Config, error) {
	data, err := os.ReadFile(filePath)
	if err != nil {
		return nil, err
	}
	var cfg Config
	err = json.Unmarshal(data, &cfg)
	if err != nil {
		return nil, err
	}
	return &cfg, nil
}
