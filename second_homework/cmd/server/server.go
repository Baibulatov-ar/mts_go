package main

import (
	"context"
	"errors"
	"flag"
	"fmt"
	"golang.org/x/sync/errgroup"
	"net/http"
	"os"
	"os/signal"
	"second_homework/internal/config"
	"second_homework/internal/server_handlers"
	"syscall"
	"time"
)

const shutdownTimeout = 15 * time.Second

func main() {
	cfg, err := ReadConfig()

	if err != nil {
		fmt.Println(fmt.Errorf("fatal: init config %v", err))
		os.Exit(1)
	}

	var mux = http.NewServeMux()
	var srv = &http.Server{
		Addr:    cfg.Port,
		Handler: mux,
	}

	handler := server_handlers.NewHandler(*cfg)

	mux.HandleFunc("/version", handler.GetApiVersion)
	mux.HandleFunc("/decode", handler.PostDecode)
	mux.HandleFunc("/hard-op", handler.GetHardOp)

	ctx, stop := signal.NotifyContext(context.Background(), syscall.SIGINT, syscall.SIGTERM)
	defer stop()

	group, ctx := errgroup.WithContext(ctx)

	group.Go(func() (err error) {
		if err = srv.ListenAndServe(); err != nil && !errors.Is(err, http.ErrServerClosed) {
			return fmt.Errorf("failed to serve http-server_handlers: %w", err)
		}
		return nil
	})

	group.Go(func() (err error) {
		<-ctx.Done()

		shutdownCtx, cancel := context.WithTimeout(context.Background(), shutdownTimeout)
		defer cancel()

		err = srv.Shutdown(shutdownCtx)
		if err != nil {
			return err
		}

		return nil
	})

	err = group.Wait()
	if err != nil {
		fmt.Printf("after wait: %s\n", err)
		return
	}

}

func ReadConfig() (*config.Config, error) {
	var configPath string
	flag.StringVar(&configPath, "c", "/Users/a4m1r/GolandProjects/mts_go/second_homework/configs/configs.json", "set configs path")
	flag.Parse()
	return config.NewConfig(configPath)
}
