package main

import (
	"net/http"
	"second_homework/internal/client"
	"time"
)

func main() {
	urls := []string{
		"http://localhost:8080/version",
		"http://localhost:8080/decode",
		"http://localhost:8080/hard-op",
	}
	c := client.NewClient(&http.Client{Timeout: time.Duration(15) * time.Second}, urls)
	c.SendRequests()
}
