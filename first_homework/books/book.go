package books

type Book struct {
	Title         string
	Author        string
	NumberOfPages int
	YearOfIssue   int
}

func GetTitle(book Book) string {
	return book.Title
}

func GetAuthor(book Book) string {
	return book.Author
}

func GetNumberOfPages(book Book) int {
	return book.NumberOfPages
}

func GetYearOfIssue(book Book) int {
	return book.YearOfIssue
}
