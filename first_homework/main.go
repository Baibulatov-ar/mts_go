package main

import (
	"awesomeProject5/books"
	"awesomeProject5/library"
	storage3 "awesomeProject5/storage"
	"fmt"
)

func main() {
	arr := []books.Book{
		books.Book{"Oblomov", "Ivan Goncharov", 640, 1859},
		books.Book{"Hero of our time", "Mikhail Lermontov", 224, 1839},
		books.Book{"War and Peace", "Lev Tolstoy", 1300, 1867},
		books.Book{"Crime and Punishment", "Fedor Dostoevsky", 608, 1866},
		books.Book{"Fathers and Sons", "Ivan Turgenev", 288, 1866},
	}
	storage1 := storage3.StorageConstruct(make(map[int]books.Book))
	liba1 := library.LibraryConstruct(&storage1, library.IdFromNameMap)
	for i := 0; i < 5; i++ {
		liba1.AddBook(&arr[i])
	}
	fmt.Println(liba1.FindBook("Oblomov"))
	fmt.Println(liba1.FindBook("Crime and Punishment"))
	storage2 := storage3.StorageSliceConstruct(make([]books.Book, 0, 0))
	liba2 := library.LibrarySliceConstruct(&storage2, library.IdFromNameSlice)
	for i := 0; i < 5; i++ {
		liba2.AddBook(&arr[i])
	}
	fmt.Println(liba2.FindBook("Hero of our time"))
	fmt.Println(liba2.FindBook("Fathers and Sons"))
}
