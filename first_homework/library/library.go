package library

import (
	book "awesomeProject5/books"
	"awesomeProject5/storage"
	"hash/fnv"
)

func IdFromNameMap(Title string) uint32 {
	h := fnv.New32a()
	h.Write([]byte(Title))
	return (h.Sum32())
}

func IdFromNameSlice(Title string) uint32 {
	h := fnv.New32()
	h.Write([]byte(Title))
	return (h.Sum32() % 1838384)
}

type Library struct {
	storage storage.Storage
	GetId   func(string string) uint32
}

func LibraryConstruct(memory storage.Storage, searchOfId func(string) uint32) Library {
	return Library{
		storage: memory,
		GetId:   searchOfId,
	}
}

func LibrarySliceConstruct(memory storage.Storage, searchOfId func(string) uint32) Library {
	return Library{
		storage: memory,
		GetId:   searchOfId,
	}
}

func (l *Library) AddBook(Book *book.Book) {
	id := l.GetId(Book.Title)
	l.storage.AddId(int(id), Book)
}

func (l *Library) FindBook(NameOfBook string) book.Book {
	id := l.GetId(NameOfBook)
	return l.storage.GetName(int(id))
}
