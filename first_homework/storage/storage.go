package storage

import book "awesomeProject5/books"

type Storage interface {
	AddId(id int, book *book.Book)
	GetName(id int) book.Book
}
