package storage

import (
	book "awesomeProject5/books"
)

type StorageSlice struct {
	memory []book.Book
}

func StorageSliceConstruct([]book.Book) StorageSlice {
	return StorageSlice{
		make([]book.Book, 0, 0),
	}
}

func (str *StorageSlice) AddId(id int, Book *book.Book) {
	tmp := str.memory
	str.memory = make([]book.Book, int(max(float64(id), float64(len(tmp)))+1), int(max(float64(id), float64(len(tmp)))+1)*2)
	for i := 0; i < len(tmp); i++ {
		str.memory[i] = tmp[i]
	}
	str.memory[id] = *Book
}

func (str *StorageSlice) GetName(id int) book.Book {
	return str.memory[id]
}
