package storage

import (
	book "awesomeProject5/books"
)

type StorageMap struct {
	memory map[int]book.Book
}

func StorageConstruct(map[int]book.Book) StorageMap {
	return StorageMap{
		memory: make(map[int]book.Book),
	}
}

func (str *StorageMap) AddId(id int, book *book.Book) {
	str.memory[id] = *book
}

func (str *StorageMap) GetName(id int) book.Book {
	return str.memory[id]
}
